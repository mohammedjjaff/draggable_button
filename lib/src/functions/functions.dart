import 'dart:ui';
import 'package:shared_preferences/shared_preferences.dart';

class Functions {

  /// in here we save default position when app start
  saveDefaultPosition(Offset offset)async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setDouble('dx', offset.dx);
    await preferences.setDouble('dy', offset.dy);
  }

  /// in here we get default position from shared preferences when app start
  Future<Offset?> loadDefaultPosition()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    double dx = preferences.getDouble("dx") ?? 0.0;
    double dy = preferences.getDouble("dy") ?? 0.0;
    Offset defaultPosition = Offset(dx, dy);
    if(preferences.getDouble("dx") == null){
      return null;
    }else{
      return defaultPosition;
    }
  }


}