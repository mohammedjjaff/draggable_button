import 'package:flutter/material.dart';

import '../functions/functions.dart';

class DraggableButton extends StatefulWidget {
  final Offset? defaultPosition;
  final Color? color;
  final Widget child;
  final double? size;
  final double? radius;
  final VoidCallback? onTap;
  const DraggableButton({
    super.key,
    this.defaultPosition,
    this.color,
    required this.child,
    this.size,
    this.radius,
    this.onTap
  });

  @override
  State<DraggableButton> createState() => _DraggableButtonState();
}

class _DraggableButtonState extends State<DraggableButton> {


  late Offset position;
  Functions functions = Functions();
  @override
  void initState() {
    setDefaultPosition();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      duration: const Duration(milliseconds: 50),
      left: position.dx,
      top: position.dy,
      child: GestureDetector(
        onPanUpdate: (positionUpdate) async {
          setState(() {
            position = Offset(positionUpdate.globalPosition.dx - 30, positionUpdate.globalPosition.dy - 170);
          });
          await functions.saveDefaultPosition(positionUpdate.globalPosition);
        },
        onPanEnd: (positionEnd) async {
          final screenWidth = MediaQuery.of(context).size.width;
          if (position.dx < screenWidth / 2) {
            setState(() {
              position = Offset(10, position.dy);
            });
          } else {
            setState(() {
              position = Offset(screenWidth - ((widget.size ?? 80) + 10), position.dy);
            });
          }
          await functions.saveDefaultPosition(position);
        },
        onTap: widget.onTap ?? (){},
        child: Container(
          width: widget.size ?? 80,
          height: widget.size ?? 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(widget.radius ?? 1000),
            color: widget.color ?? Colors.deepOrange,
            boxShadow: const [
              BoxShadow(
                color: Color(0x3F101010),
                blurRadius: 24,
                offset: Offset(4, 8),
                spreadRadius: 0,
              )
            ],
          ),
          child: Center(
            child: widget.child,
          ),
        ),
      ),
    );
  }



  setDefaultPosition() async {
    position = Offset.zero;
    Offset? defaultPosition = widget.defaultPosition ?? const Offset(0.0, 0.0);
    position = await functions.loadDefaultPosition() ?? defaultPosition;
    setState(() {});
  }
}

