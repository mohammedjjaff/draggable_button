
# Draggable Button

A great addition to any application,
A smoothly moving button within the application that the user can interact with and place anywhere on the screen.

## Getting Started

To install, add it to your pubspec.yaml file:

```yaml
  dependencies:
    draggable_button:

```
```dart
  import 'package:draggable_button/draggable_button.dart';

```


## Usage/Examples

```dart
import 'package:draggable_button/draggable_button.dart';

return Scaffold(
      appBar: AppBar(),
      body: Stack(
        children: [
          DraggableButton(
            color: Colors.white,
            size: 80,
            radius: 100,
            defaultPosition: const Offset(0.0, 0.0),
            onTap: (){},
            child: const Icon(Icons.phone,color: Colors.white,),
          )
        ],
      ),
    );
```


## Screenshots

![App Screenshot](https://firebasestorage.googleapis.com/v0/b/debtbook-467d2.appspot.com/o/Simulator%20Screenshot%20-%20iPhone%2015%20Pro%20Max%20-%202024-03-18%20at%2010.42.25.png?alt=media&token=57106039-f9c7-4d81-a83c-2596222aa3b0)

![App Screenshot](https://firebasestorage.googleapis.com/v0/b/debtbook-467d2.appspot.com/o/Simulator%20Screenshot%20-%20iPhone%2015%20Pro%20Max%20-%202024-03-18%20at%2010.42.10.png?alt=media&token=fa2114c4-76f4-49c6-b78b-9fc737e1fec3)


## Support

For support, email info@mohammed-aljaf.com .


## Follow me at Instagram 


Instagram : https://instagram.com/m9_6m?igshid=YTQwZjQ0NmI0OA==


