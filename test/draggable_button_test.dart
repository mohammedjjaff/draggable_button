import 'package:draggable_button/draggable_button.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyHomePage(),
    );
  }
}


class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Stack(
        children: [
          DraggableButton(
            color: Colors.white,
            size: 80,
            radius: 100,
            defaultPosition: const Offset(0.0, 0.0),
            onTap: (){},
            child: const Icon(Icons.phone,color: Colors.white,),
          )
        ],
      ),
    );
  }
}
